from comms_wrapper import Arduino


class YourCustomSensor:
    def __init__(self, port, baudrate) -> None:
        self.sensor = Arduino("Your custom sensor", port, baudrate)
        self.sensor.connect_and_handshake()

    def read_sensor(self):
        self.sensor.receive_message()
        msg = self.sensor.receivedMessages

        output = {}
        for key, value in zip(msg.keys(), msg.values()):
            output[key] = float(value)

        return output