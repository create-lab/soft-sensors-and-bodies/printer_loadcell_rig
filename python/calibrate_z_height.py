from comms_wrapper import Key
from loadcell_test_rig import Loadcell_test_rig
from custom_sensors import *
from utility import *
import os, json
from time import time, sleep

def main():
    # Get config information
    dir_path = os.path.dirname(os.path.realpath(__file__))
    config = json.load(open(dir_path + '/config.json'))

    # Connect Rig
    rig = Loadcell_test_rig(config["printer"]["port"], config["printer"]["baudrate"], 
                            config["loadcell"]["port"], config["loadcell"]["baudrate"], 
                            config["printer"]["home"]["x"], config["printer"]["home"]["y"], config["printer"]["home"]["z"])


    key = Key()
    height = 0
    increment = 1
    while 1:
        if key.keyPress == "w":
            height += increment

        if key.keyPress == "s":
            height -= increment
        
        rig.move_axis(z = height)

        if key.keyPress == "p":
            rig.read_position(printMsg=True)

if __name__ == "__main__":
    main()