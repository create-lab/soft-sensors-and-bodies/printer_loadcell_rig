from loadcell_test_rig import Loadcell_test_rig
from custom_sensors import YourCustomSensor
from utility import *
import os, json

def main():
    # Get config information
    dir_path = os.path.dirname(os.path.realpath(__file__))
    config = json.load(open(dir_path + '/config.json'))

    # Connect sensor
    your_custom_sensor = YourCustomSensor(config["sensor"]["port"], config["sensor"]["baudrate"])

    # Connect Rig
    rig = Loadcell_test_rig(config["printer"]["port"], config["printer"]["baudrate"], 
                            config["loadcell"]["port"], config["loadcell"]["baudrate"], 
                            config["printer"]["home"]["x"], config["printer"]["home"]["y"], config["printer"]["home"]["z"])

    # Query data save location and filename
    save_dir = define_csv_save_location(config["logging"]["relativeDirectory"], 
                                        config["logging"]["expSetName"])

    while 1:
        filename = obtain_csv_filename(save_dir)

        data_capture = {"time":[], "Fv":[], "Fh":[], "dist":[], "s1":[]} 
        
        # NOTE: Change 'load_threshold' to change the maximum normal force for the probe to go down to
        rig.begin_probing_down(zdot=1, load_threshold=100)

        timer = time() 
        while 1:
            # Collect data
            t = time() - timer
            Fv, Fh = rig.read_loacell()
            sensor = your_custom_sensor.read_sensor()
            dist = rig.zdot*(time() - rig.probe_timer)

            # Store data
            data_capture["time"].append(t)
            data_capture["Fv"].append(Fv)
            data_capture["Fh"].append(Fh)
            data_capture["dist"].append(dist)
            data_capture["s1"].append(sensor["s1"])   

            # Check if probing must stop
            if rig.is_probe_limit_reached():
                break

        # Plot and save data
        plot_and_save_data(plottingData= ([data_capture["dist"], data_capture["Fv"]], 
                                [data_capture["dist"], data_capture["s1"]]),
                                xAxisLabel= ("Distance (mm)", "Distance (mm)"), 
                                yAxisLabel =("Thrust up (g)", "Sensor"),
                                label = (["Loadcell up"], ["Sensor"]), 
                                savingData= (data_capture["time"], data_capture["Fv"], data_capture["Fh"], data_capture["dist"], data_capture["s1"]), 
                                savingData_names = ["time", "fv", "fh", "dist", "s1"],
                                filename= filename,
                                saveDir= save_dir,
                                display_plot= True, 
                                saveData = True, 
                                figsize = (6,8))
        
        rig.move_home()

if __name__ == "__main__":
    main()